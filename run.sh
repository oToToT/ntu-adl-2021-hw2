#!/bin/bash

set -x

python3.8 predict_choice.py '--model_name_or_path' 'models/qa_selection_model' \
    '--cache_dir' './cache' '--output_dir' 'models/qa_selection_model' '--eval_file' \
    "$2" '--do_eval' '--context_file' \
    "$1" '--max_seq_length' '512' '--save_path' 'selected_context.json'

python3.8 train_qa.py '--model_name_or_path' 'models/qa_long_model' \
    '--do_eval' '--per_device_train_batch_size' '12' '--learning_rate' '3e-5' \
    '--num_train_epochs' '5' '--max_seq_length' '512' \
    '--gradient_accumulation_steps' '64' '--doc_stride' '128' \
    '--output_dir' 'results/' '--cache_dir' './cache' '--validation_file' \
    'selected_context.json'

rm selected_context.json
mv results/eval_predictions.json "$3"
