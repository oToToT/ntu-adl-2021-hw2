#!/usr/bin/env python
# coding=utf-8
# Copyright The HuggingFace Team and The HuggingFace Inc. team.
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Fine-tuning the library models for multiple choice.
"""

import logging
import random
import sys
import json
from dataclasses import dataclass, field
from typing import Optional, Union

from tqdm import tqdm
import torch
from torch.utils.data.dataloader import DataLoader
from datasets import Dataset

from accelerate import Accelerator
import transformers
from transformers import (
    AutoConfig,
    AutoModelForMultipleChoice,
    AutoTokenizer,
    HfArgumentParser,
    TrainingArguments,
    default_data_collator,
    set_seed,
)
from transformers.file_utils import PaddingStrategy
from transformers.tokenization_utils_base import PreTrainedTokenizerBase
from transformers.trainer_utils import is_main_process

logger = logging.getLogger(__name__)


@dataclass
class ModelArguments:
    """
    Arguments pertaining to which model/config/tokenizer we are going to
    fine-tune from.
    """

    model_name_or_path: str = field(
        metadata={
            "help":
            "Path to pretrained model or model identifier from "
            "huggingface.co/models"
        })
    config_name: Optional[str] = field(
        default=None,
        metadata={
            "help":
            "Pretrained config name or path if not the same as model_name"
        })
    tokenizer_name: Optional[str] = field(
        default=None,
        metadata={
            "help":
            "Pretrained tokenizer name or path if not the same as model_name"
        })
    cache_dir: Optional[str] = field(
        default=None,
        metadata={
            "help":
            "Where do you want to store the pretrained models downloaded "
            "from huggingface.co"
        },
    )
    use_fast_tokenizer: bool = field(
        default=True,
        metadata={
            "help":
            "Whether to use one of the fast tokenizer (backed by the "
            "tokenizers library) or not."
        },
    )
    model_revision: str = field(
        default="main",
        metadata={
            "help":
            "The specific model version to use (can be a branch name, tag "
            "name or commit id)."
        },
    )
    use_auth_token: bool = field(
        default=False,
        metadata={
            "help":
            "Will use the token generated when running"
            "`transformers-cli login` (necessary to use this script with"
            "private models)."
        },
    )


@dataclass
class DataTrainingArguments:
    """
    Arguments pertaining to what data we are going to input our model for
    training and eval.
    """

    eval_file: str = field(
        metadata={"help": "The input training data file (a text file)."})
    save_path: str = field(
        metadata={"help": "The path to save the result data"})
    context_file: str = field(
        metadata={"help": "The input context data file (a text file)."})
    overwrite_cache: bool = field(
        default=False,
        metadata={"help": "Overwrite the cached training and evaluation sets"})

    preprocessing_num_workers: Optional[int] = field(
        default=None,
        metadata={
            "help": "The number of processes to use for the preprocessing."
        },
    )
    max_seq_length: int = field(
        default=None,
        metadata={
            "help":
            "The maximum total input sequence length after tokenization. If"
            "passed, sequences longer than this will be truncated, sequences"
            "shorter will be padded."
        },
    )
    pad_to_max_length: bool = field(
        default=False,
        metadata={
            "help":
            "Whether to pad all samples to the maximum sentence length. "
            "If False, will pad the samples dynamically when batching to the"
            "maximum length in the batch. More efficient on GPU but very bad"
            "for TPU."
        },
    )

    def __post_init__(self):
        extension = self.eval_file.split(".")[-1]
        assert extension in ["csv", "json"
                             ], "`eval_file` should be a csv or a json file."


@dataclass
class DataCollatorForMultipleChoice:
    """
    Data collator that will dynamically pad the inputs for multiple choice
    received.

    Args:
        tokenizer (:class:`~transformers.PreTrainedTokenizer` or
                :class:`~transformers.PreTrainedTokenizerFast`):
            The tokenizer used for encoding the data.
        padding (:obj:`bool`, :obj:`str` or
                 :class:`~transformers.file_utils.PaddingStrategy`,
                 `optional`, defaults to :obj:`True`):
            Select a strategy to pad the returned sequences (according to the
            model's padding side and padding index) among:

            * :obj:`True` or :obj:`'longest'`: Pad to the longest sequence in
              the batch (or no padding if only a single sequence if provided).
            * :obj:`'max_length'`: Pad to a maximum length specified with the
              argument :obj:`max_length` or to the maximum acceptable input
              length for the model if that argument is not provided.
            * :obj:`False` or :obj:`'do_not_pad'` (default): No padding (i.e.,
              can output a batch with sequences of different lengths).
        max_length (:obj:`int`, `optional`):
            Maximum length of the returned list and optionally padding length
            (see above).
        pad_to_multiple_of (:obj:`int`, `optional`):
            If set will pad the sequence to a multiple of the provided value.

            This is especially useful to enable the use of Tensor Cores on
            NVIDIA hardware with compute capability >= 7.5 (Volta).
    """

    tokenizer: PreTrainedTokenizerBase
    padding: Union[bool, str, PaddingStrategy] = True
    max_length: Optional[int] = None
    pad_to_multiple_of: Optional[int] = None

    def __call__(self, features):
        labels = [feature.pop("relevant") for feature in features]
        ids = [feature.pop("id") for feature in features]
        questions = [feature.pop("question") for feature in features]
        paragraphs = [feature.pop("paragraphs") for feature in features]

        batch_size = len(features)
        num_choices = len(features[0]["input_ids"])
        flattened_features = [[{k: v[i]
                                for k, v in feature.items()}
                               for i in range(num_choices)]
                              for feature in features]
        flattened_features = sum(flattened_features, [])

        batch = self.tokenizer.pad(
            flattened_features,
            padding=self.padding,
            max_length=self.max_length,
            pad_to_multiple_of=self.pad_to_multiple_of,
            return_tensors="pt",
        )

        # Un-flatten
        batch = {
            k: v.view(batch_size, num_choices, -1)
            for k, v in batch.items()
        }
        # Add back labels
        batch["labels"] = torch.tensor(labels, dtype=torch.int64)
        batch["ids"] = ids
        batch["paragraphs"] = paragraphs
        batch["questions"] = questions
        return batch


def main():
    # See all possible arguments in src/transformers/training_args.py
    # or by passing the --help flag to this script.
    # We now keep distinct sets of args, for a cleaner separation of concerns.

    parser = HfArgumentParser(
        (ModelArguments, DataTrainingArguments, TrainingArguments))
    model_args, data_args, training_args = parser.parse_args_into_dataclasses()

    # Setup logging
    logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        handlers=[logging.StreamHandler(sys.stdout)],
    )
    logger.setLevel(logging.INFO if is_main_process(training_args.local_rank
                                                    ) else logging.WARN)

    # Log on each process the small summary:
    logger.warning(
        f"Process rank: {training_args.local_rank}, device: "
        f"{training_args.device}, n_gpu: {training_args.n_gpu}"
        f"distributed training: {bool(training_args.local_rank != -1)}, "
        f"16-bits training: {training_args.fp16}")
    # Set the verbosity to info of the Transformers logger
    # (on main process only):
    if is_main_process(training_args.local_rank):
        transformers.utils.logging.set_verbosity_info()
        transformers.utils.logging.enable_default_handler()
        transformers.utils.logging.enable_explicit_format()
    logger.info(f"Training/evaluation parameters {training_args}")

    # Set seed before initializing model.
    set_seed(training_args.seed)

    # Get the datasets: you can either provide your own CSV/JSON/TXT training and evaluation files (see below)
    # or just provide the name of one of the public datasets available on the hub at https://huggingface.co/datasets/
    # (the dataset will be downloaded automatically from the datasets Hub).

    # For CSV/JSON files, this script will use the column called 'text' or the first column if no column called
    # 'text' is found. You can easily tweak this behavior (see below).

    def transform_data(data, num_contexts):
        mx_paragraphs = 0
        for ele in data:
            mx_paragraphs = max(mx_paragraphs, len(ele['paragraphs']))
            del ele['answers']
        for ele in data:
            while len(ele['paragraphs']) < mx_paragraphs:
                ele['paragraphs'].append(-1)
            ele['relevant'] = ele['paragraphs'].index(ele['relevant'])
        keys = set()
        for ele in data:
            for key in ele.keys():
                keys.add(key)
        transformed = {key: [] for key in keys}
        for ele in data:
            for (k, v) in ele.items():
                transformed[k].append(v)
        return Dataset.from_dict(transformed)

    context_data = json.load(open(data_args.context_file, "r"))
    raw_data = json.load(open(data_args.eval_file, "r"))
    for data in raw_data:
        data['relevant'] = data['paragraphs'][0]
        data['answers'] = {'text': "", "answer_start": 0}
    dataset = transform_data(raw_data, len(context_data))

    # Load pretrained model and tokenizer

    # Distributed training:
    # The .from_pretrained methods guarantee that only one local process can
    # concurrently download model & vocab.
    config = AutoConfig.from_pretrained(
        model_args.config_name
        if model_args.config_name else model_args.model_name_or_path,
        cache_dir=model_args.cache_dir,
        revision=model_args.model_revision,
        use_auth_token=True if model_args.use_auth_token else None,
    )
    tokenizer = AutoTokenizer.from_pretrained(
        model_args.tokenizer_name
        if model_args.tokenizer_name else model_args.model_name_or_path,
        cache_dir=model_args.cache_dir,
        use_fast=model_args.use_fast_tokenizer,
        revision=model_args.model_revision,
        use_auth_token=True if model_args.use_auth_token else None,
    )
    model = AutoModelForMultipleChoice.from_pretrained(
        model_args.model_name_or_path,
        from_tf=bool(".ckpt" in model_args.model_name_or_path),
        config=config,
        cache_dir=model_args.cache_dir,
        revision=model_args.model_revision,
        use_auth_token=True if model_args.use_auth_token else None,
    )
    accelerator = Accelerator()

    if data_args.max_seq_length is None:
        max_seq_length = tokenizer.model_max_length
        if max_seq_length > 1024:
            logger.warn(
                "The tokenizer picked seems to have a very large "
                f"`model_max_length` ({tokenizer.model_max_length}). "
                "Picking 1024 instead. You can change that default value by "
                "passing --max_seq_length xxx.")
            max_seq_length = 1024
    else:
        if data_args.max_seq_length > tokenizer.model_max_length:
            logger.warn(
                f"The max_seq_length passed ({data_args.max_seq_length}) is "
                "larger than the maximum length for the"
                f"model ({tokenizer.model_max_length}). Using "
                f"max_seq_length={tokenizer.model_max_length}.")
        max_seq_length = min(data_args.max_seq_length,
                             tokenizer.model_max_length)

    # Preprocessing the datasets.
    def preprocess_function(examples):
        first_sentences = [[context] * len(examples['paragraphs'][i])
                           for i, context in enumerate(examples['question'])]
        second_sentences = [[
            context_data[context_id] if context_id != -1 else "欸"
            for context_id in contexts
        ] for contexts in examples['paragraphs']]
        lens = [0]
        for contexts in examples['paragraphs']:
            lens.append(lens[-1] + len(contexts))

        # Flatten out
        first_sentences = sum(first_sentences, [])
        second_sentences = sum(second_sentences, [])

        # Tokenize
        tokenized_examples = tokenizer(
            first_sentences,
            second_sentences,
            truncation=True,
            max_length=max_seq_length,
            padding="max_length" if data_args.pad_to_max_length else False,
        )
        # Un-flatten
        return {
            k: [v[lens[i]:lens[i + 1]] for i in range(0,
                                                      len(lens) - 1)]
            for k, v in tokenized_examples.items()
        }

    dataset = dataset.map(
        preprocess_function,
        batched=True,
        num_proc=data_args.preprocessing_num_workers,
        load_from_cache_file=not data_args.overwrite_cache,
    )

    # Data collator
    data_collator = (default_data_collator if data_args.pad_to_max_length else
                     DataCollatorForMultipleChoice(
                         tokenizer=tokenizer,
                         pad_to_multiple_of=8 if training_args.fp16 else None))

    dataloader = DataLoader(dataset, collate_fn=data_collator, batch_size=32)
    model.to(accelerator.device)
    model, dataloader = accelerator.prepare(model, dataloader)
    model.eval()

    result = {'data': []}

    for batch in tqdm(dataloader):
        questions = batch.pop("questions")
        ids = batch.pop("ids")
        paragraphs = batch.pop("paragraphs")
        with torch.no_grad():
            outputs = model(**batch)
        predictions = outputs.logits.argmax(dim=-1)

        for i, idx in enumerate(ids):
            result['data'].append({
                'id': idx,
                'question': questions[i],
                'context': context_data[paragraphs[i][predictions[i]]],
                'answers': [{'text': '', 'answer_start': 0}]
            })
    json.dump(result, open(data_args.save_path, "w"))


def _mp_fn(index):
    # For xla_spawn (TPUs)
    main()


if __name__ == "__main__":
    main()
