# Homework 2 - ADL (NTU 2021 Spring)

## Environment

```bash
pip install -r requirement.txt
bash download_dataset.sh
```

## Training

To train context selection model, you could run the command below. It will output the trained model to `models/macbert_qa_selection`.

```bash
python3.8 train_choice.py --model_name_or_path hfl/chinese-macbert-base
    --cache_dir ./cache --output_dir models/macbert_qa_selection --train_file
    hw2_dataset/dataset/train.json --do_train --do_eval --context_file
    hw2_dataset/dataset/context.json --max_seq_length 512
    --gradient_accumulation_steps 128 --num_train_epochs 5
    --num_selected_contexts 7 --save_steps 10 --per_device_train_batch_size 2
```

To train question answering model, you could run the command below. It will output the trained model to `models/macbert_question_answer`.


First, we need to transform our dataset to the form similar to SQuAD.
```bash
python3.8 transform_qa.py --input hw2_dataset/dataset/train.json --context hw2_dataset/dataset/context.json --output train_qa.json
```

Then, we could perfrom the training.
```bash
python3.8 train_qa.py --model_name_or_path hfl/chinese-macbert-base --gradient_accumulation_steps 64 --do_train --per_device_train_batch_size 12 --learning_rate 3e-5 --num_train_epochs 20 --doc_stride 128 --output_dir models/macbert_question_answer --max_seq_length 512 --cache_dir ./cache --train_file train_qa.json --save_steps 10
```
