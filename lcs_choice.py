"""
Transform training data to qa trainable files
"""

from pathlib import Path
import argparse
import json

from tqdm import tqdm


def lcs(s1, s2):
    """
    calculate longest common subsequence of s1 and s2
    """
    len_s1, len_s2 = len(s1), len(s2)
    dp = [[0 for j in range(len_s2 + 1)] for i in range(len_s1 + 1)]
    ans = 0
    for i in range(1, len_s1 + 1):
        for j in range(1, len_s2 + 1):
            if s1[i - 1] == s2[j - 1]:
                dp[i][j] = dp[i - 1][j - 1] + 1
            else:
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1])
            ans = max(ans, dp[i][j])
    return ans


def kgrams(input_s, k):
    """
    return k-gram set from input_s
    """
    return set(input_s[i:i + k] for i in range(len(input_s) - k))


def best_rel(data, context):
    """
    find best paragraph with greatest relevance
    """

    best_id, best_scr = 0, -1
    for par in data['paragraphs']:
        lcs_scr = lcs(data['question'], context[par])
        qset2, cset2 = kgrams(data['question'], 2), kgrams(context[par], 2)
        qset3, cset3 = kgrams(data['question'], 3), kgrams(context[par], 3)

        scr = len(qset3 & cset3) * 4 + len(qset2 & cset2) * 12 + lcs_scr
        if scr > best_scr:
            best_id, best_scr = par, scr

    return best_id, best_scr


def main(args):
    """
    load and transform indata to outdata
    """
    context = json.load(open(args.context))
    indata = json.load(open(args.input))
    res = {'data': []}
    correct, total = 0, 0
    # ctr = 0
    for data in tqdm(indata):
        best_context, best_score = best_rel(data, context)
        # if best_context != data['relevant']:
        # ctr += 1
        # if ctr == 4:
        # print(len(data['question']))
        # print(data['question'])
        # print(best_score)
        # print(context[best_context])
        # print(context[data['relevant']])
        # return

        correct += best_context == data['relevant']
        total += 1

        cur = {
            'question': data['question'],
            'context': context[best_context],
            'id': data['id']
        }
        if 'answers' in data:
            cur['answers'] = {
                'text': [ans['text'] for ans in data['answers']],
                'answer_start': [ans['start'] for ans in data['answers']]
            }
        res['data'].append(cur)
    json.dump(res, open(args.output, 'w'))
    print(f'correct = {correct}, total = {total}, acc = {correct / total}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--context', type=Path, help='Context json file')
    parser.add_argument('--input', type=Path, help='Input json file')
    parser.add_argument('--output', type=Path, help='Output json file')

    main(parser.parse_args())
