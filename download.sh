mkdir -p models

wget https://adl-models.ototot.tk/hw2/qa_long_model.tar.gz -O qa_long_model.tar.gz
tar xvf qa_long_model.tar.gz
mv question_answer_long models/qa_long_model

wget https://adl-models.ototot.tk/hw2/qa_selection_model.tar.gz -O qa_selection_model.tar.gz
tar xvf qa_selection_model.tar.gz
mv macbert_qa_selection models/qa_selection_model
