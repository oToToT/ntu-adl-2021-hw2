#!/bin/bash

if [ ! -f "dataset.zip" ]
then
    wget 'https://adl-models.ototot.tk/hw2/dataset.zip'
fi

if [ ! -d 'hw2_dataset/' ]
then
    unzip dataset.zip
fi
python3.8 -m spacy download zh_core_web_md
