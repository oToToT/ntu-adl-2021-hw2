"""
Transform training data to qa trainable files
"""

from pathlib import Path
import argparse
import json
from tqdm import tqdm


def main(args):
    """
    load and transform indata to outdata
    """
    context = json.load(open(args.context))
    indata = json.load(open(args.input))
    res = {'data': []}
    for data in tqdm(indata):
        cur = {
            'question': data['question'],
            'context': context[data["relevant"]],
            'id': data['id']
        }
        if 'answers' in data:
            cur['answers'] = {
                'text': [ans['text'] for ans in data['answers']],
                'answer_start': [ans['start'] for ans in data['answers']]
            }
        res['data'].append(cur)
    json.dump(res, open(args.output, 'w'))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--context', type=Path, help='Context json file')
    parser.add_argument('--input', type=Path, help='Input json file')
    parser.add_argument('--output', type=Path, help='Output json file')

    main(parser.parse_args())
